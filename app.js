#!/usr/bin/node

var express =       require('express');
var http =          require('http');
var https =         require('https');
var path =          require('path');
var server =        require('socket.io');
var Convert =       require('ansi-to-html');

var convert = new Convert({ bg: '#000', fg: '#FFF', newline: false });
var app = express();

var TelnetSocket, net;

var TTYPE = 24;
var GMCP = 201;
var CUSTOM_CLIENT = 222;
var IAC = 255;
var SB = 250;
var SE = 240;
var IACSBGMCP = [IAC, SB, GMCP];
var IACSE = [IAC, SE];
var IACSBTTYPE = [IAC, SB, TTYPE];
var ISANSI = [IAC, SB, TTYPE, 0, 65, 78, 83, 73, IAC, SE];

({TelnetSocket} = require("telnet-stream"));

net = require("net");

app.use(express.static(path.join(__dirname)));

app.get('/', function (req, res) {
    res.sendFile('index.html');
});

app.get("/testIP", function(req, res) {
    let opts = {
        host: "api.vpnblocker.net",
        path: "/v2/json/" + "52.15.57.217"
    }
    callback = function(resp) {
        let data = "";
        resp.on("data", (chunk) => {
            data += chunk;
        });

        resp.on("end", () => {
            res.setHeader("Content-Type", "application/json");
            res.end(data);
        })
    }
    http.request(opts, callback).end();
});

var httpserv = http.createServer(app).listen(3000, function() {
    console.log("listening on 3000");
});

var io = server(httpserv, {path: '/socket.io', pingTimeout: 25000, pingInterval: 5000, reconnection: false});

io.on('connection', function(socket) {

    socket.reconnects = false;

    var s = net.createConnection(4000, "localhost");
    var telnet = new TelnetSocket(s, { bufferSize: 2048 });

    var addr = socket.handshake.headers['x-forwarded-for'];

    if (addr) {
        let opts = {
            host: "api.vpnblocker.net",
            path: "/v2/json/" + addr
        }
        callback = function(resp) {
            let data = "";
            resp.on("data", (chunk) => {
                data += chunk;
            });

            resp.on("end", () => {
                let info = JSON.parse(data);
                if (info["host-ip"]) {
                    let msg = "Unable to connect to server.\nError code: IX-91055\n\n";
                    b = Buffer.alloc(msg.length, msg);
                    read(socket, b);
                    s.end();
                    socket.disconnect();
                }
            });
        }
        let req = http.request(opts, callback);
        req.on("error", (e) => {
            let msg = "Unable to connect to server.\nError code: UX-91076\n\n" + e.message + "\n\n";
            b = Buffer.alloc(msg.length, msg);
            read(socket, b);
            s.end();
            socket.disconnect();
        });
        req.end();
    } else {
        let msg = "There was a problem connecting to the server. If the issue persists, please provide feedback at https://olmran.net\n\n";
        b = Buffer.alloc(msg.length, msg);
        read(socket, b);
        s.end();
        socket.disconnect();
    }

    s.on("end", function() {
        let msg = "You've been disconnected from the server.";
        let b = Buffer.alloc(msg.length, msg);
        read(socket, b);
        socket.disconnect();
    });

    s.on("error", function(err) {
        let msg = "Unable to connect to the server. Please try again later.\n\n";
        b = Buffer.alloc(msg.length, msg);
        read(socket, b);
        s.end();
        socket.disconnect();
    });

    telnet.writeSub(GMCP, gmcpSupport());

    var ipaddr = "ipaddr " + addr;

    telnet.on("sub", function(option, buffer) {
        if (option == GMCP) {
            // console.log(buffer.toString("utf8"));
            var gmcp = buffer.toString("utf8");
            var split = gmcp.toString("utf8").split(/\s(.+)/);
            var jsonString, data;
            if (split.length > 1) {
                jsonString = split[1];
                data = JSON.parse(jsonString);
            }
            if (gmcp.includes("comm.channel")) {
                socket.emit('chat', data.msg);
            } else if (gmcp.includes("room.info")) {
                socket.emit('roominfo', data.exits);
            } else if (gmcp.includes("charinfo")) {
                telnet.writeSub(GMCP, "char_base");
            } else if (gmcp.includes("char.base")) {
                socket.emit('player', data.name);
            } else if (gmcp.includes("ipaddr")) {
                telnet.writeSub(GMCP, Buffer.alloc(ipaddr.length, ipaddr))
            }
        }
    });

    telnet.on("will", function(option) {
        switch (option) {
            case GMCP:
            case CUSTOM_CLIENT:
                telnet.writeDo(option);
                break;
            default:
                telnet.writeDont(option);
        }
        
    });

    telnet.on("do", function(option) {
        telnet.writeWont(option);
    });

    telnet.on("data", function(buffer) {
        read(socket, buffer);
    });

    socket.on('message', function(msg) {
        telnet.write(msg + "\n");
    });

    socket.on('disconnect', function(data) {
        s.end();
        socket.disconnect();
    });

});

function gmcpSupport() {
    let supportedGmcp = 'core_supports_set ["comm.channel","room.info","char.base","charInfo","ipaddr"]';
    return Buffer.alloc(supportedGmcp.length, supportedGmcp); 
};

function sendTtype() {
    var z = Buffer.alloc(1);
    var ansi = Buffer.alloc(4, "ANSI");
    var length = z.length + ansi.length;
    telnet.writeSub(TTYPE, Buffer.concat([z, ansi], length));
};

function read(socket, buffer) {
    if (buffer.readInt8(0) == 0) {
        return;
    }
    socket.emit('output', convert.toHtml(buffer.toString('utf8')));
};