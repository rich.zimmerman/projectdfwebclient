var currentPlayer = undefined;
var playerData = undefined;

var playerDataSchema = {
    players: {}
}

var playerSchema = {
    // TODO: Add button settings for position/more buttons
    macros: {
        ActionButton1: "ActionButton1",
        ActionButton2: "ActionButton2",
        ActionButton3: "ActionButton3",
        ActionButton4: "ActionButton4"
    },
};

function updateMacros(player) {
    var macros = playerData.players[player]['macros'];
    for (var i = 0; i < Object.keys(macros).length; i++) {
        var key = Object.keys(macros)[i];
        var macro = macros[key];
        document.getElementById(key).text = macro;
    }
}

function setMacro(element) {
    var macro = prompt("Set macro for " + element.id);
    if (macro != undefined) {
        element.text = macro;
        playerData.players[currentPlayer]['macros'][element.id] = macro;
        window.localStorage.setItem('playerData', JSON.stringify(playerData));
    }
}

 /* Open the sidenav */
function openNav() {
    // if (currentPlayer == undefined) {
    //     return;
    // }
    document.getElementById("menu").style.width = "100%";
}

/* Close/hide the sidenav */
function closeNav() {
    document.getElementById("menu").style.width = "0";
}

$(document).ready(function () {
    $("#compass").draggable();
    $("#actionButtons").draggable();

    playerData = JSON.parse(window.localStorage.getItem("playerData"));
    if (playerData == undefined) {
        playerData = playerDataSchema;
    }
    console.log(playerData);

    var autoSneak = false;
    var inputBuffer = [];
    var inputIndex = 0;

    var maxTextLimit = 150000;

    $('#input').focus();

    $('#input').on('blur', function() {
        setTimeout(function() {
            input.focus();
            input.select();
        }, 10);
    });

    var socket = io(location.origin, {path: '/socket.io'});

    // window.onbeforeunload = function(){
    //     socket.send("logout");
    //     socket.send("y");
    //     socket.emit('disconnect', 'arbitrary');
    // }

    function updateScroll(){
        var element = document.getElementById("text");
        element.scrollTop = element.scrollHeight;
    }

    function getNextInput() {
        var input = document.getElementById('input');
        if (inputBuffer.length == 0) {
            return;
        }
        if (inputIndex == 0) {
            inputIndex = -1;
        } else if (inputIndex < 0) {
            inputIndex = inputBuffer.length-1;
        } else {
            inputIndex--;
        }
        if (inputIndex < 0) {
            input.value = "";
        } else {
            input.value = inputBuffer[inputIndex];
        }
        input.select();
    }

    function getLastInput() {
        var input = document.getElementById('input');
        if (inputBuffer.length == 0) {
            return;
        }
        if (inputIndex == inputBuffer.length-1) {
            inputIndex = -1;
            input.value = "";
        } else {
            inputIndex++;
            input.value = inputBuffer[inputIndex];
        }
        input.select();
    }

    socket.on('player', function(player) {
        if (playerData.players[player] == undefined) {
            playerData.players[player] = playerSchema;
        }
        currentPlayer = player;
        updateMacros(player);
    });

    socket.on('output', function(data) {
        // Remove first child nodes when number of child nodes >= max 'buffer' count
        var childNodes = document.getElementById("text").childElementCount;
        if (childNodes >= maxTextLimit) {
            var diff = childNodes - maxTextLimit;
            for (var i = 0; i <= diff; i++) {
                var child = document.getElementById("text").firstChild;
                child.remove();
            }
        }
        $("#text").append(data);
        updateScroll();
    });

    socket.on('chat', function(data) {
        $("#text").append('<pre><span style="color:#F00">' + data + '</span><br><br></pre>');
        updateScroll();
    });

    socket.on('roominfo', function(room) {
        var exits = ["NW", "N", "NE", "W", "E", "SW", "S", "SE"];
        for (var i = 0; i < exits.length; i++) {
            var exit = exits[i];
            if (room[exit] != undefined) {
                $("#" + exit).css("background-color", "rgba(0, 75, 255, 0.7)");
            } else {
                $("#" + exit).css("background-color", "rgba(0, 0, 255, 0.2)");
            }
        }
    });

    $('#input').on('keydown', function(e) {
        if (e.which == 40) {
            e.preventDefault();
            getLastInput();
            return;
        }

        if (e.which == 38) {
            e.preventDefault();
            getNextInput();
            return;
        }
    });

    function sendCommands(input, cmdline) {
        var auto = input.includes("&")
        var commands = input.split("&");
        for (var i = 0; i < commands.length; i++) {
            if (!auto && !cmdline) {
                document.getElementById('input').value = commands[i];
                socket.send(commands[i]);
                return;
            }
            if (commands[i] != "") {
                if (i == commands.length-1 && commands.length > 1) {
                    document.getElementById("input").value = commands[i];
                } else {
                    socket.send(commands[i]); 
                }
            }
        }
        if (commands[commands.length-1] == "" || (commands.length == 1 && (auto || cmdline))) {
            document.getElementById('input').value = "";
        }
    }

    $('#input').on('keypress', function(e) {
        if (e.which == 13) {
            var input = document.getElementById('input').value;
            if (input.length > 0) {
                if (inputBuffer.length == 10) {
                    inputBuffer.shift();
                }
                inputBuffer.push(input);
                inputIndex = -1;
                sendCommands(input, true);
            } else {
                socket.send(" ");
            }
            return;
        }

        var sneak = autoSneak ? "sneak " : "";   
        switch(e.code) {
            case "Numpad7":
                e.preventDefault();
                socket.send(sneak + "nw");
                break;
            case "Numpad8":
                e.preventDefault();
                socket.send(sneak + "n");
                break;
            case "Numpad9":
                e.preventDefault();
                socket.send(sneak + "ne");
                break;
            case "Numpad4":
                e.preventDefault();
                socket.send(sneak + "w");
                break;
            case "Numpad5":
                e.preventDefault();
                autoSneak = !autoSneak;
                var msg;
                if (autoSneak) {
                    msg = "You are now auto-sneaking.";
                } else {
                    msg = "You are no longer auto-sneaking.";
                }
                $("#text").append("<pre><span style=\"color:white;\">" + msg + "</span><br><br></pre>");
                updateScroll();
                break;
            case "Numpad6":
                e.preventDefault();
                socket.send(sneak + "e");
                break;
            case "Numpad1":
                e.preventDefault();
                socket.send(sneak + "sw");
                break;
            case "Numpad2":
                e.preventDefault();
                socket.send(sneak + "s");
                break;
            case "Numpad3":
                e.preventDefault();
                socket.send(sneak + "se");
                break;
            case "Numpad0":
                e.preventDefault();
                socket.send("look");
                break;
        }

    });

    $("#btn1").on('click', function() {
        var macro = playerData.players[currentPlayer]['macros']['ActionButton1'];
        if (macro == "") {
            return;
        }
        sendCommands(macro, false);
    });

    $("#btn2").on('click', function() {
        var macro = playerData.players[currentPlayer]['macros']['ActionButton2'];
        if (macro == "") {
            return;
        }
        sendCommands(macro, false);
    });

    $("#btn3").on('click', function() {
        var macro = playerData.players[currentPlayer]['macros']['ActionButton3'];
        if (macro == "") {
            return;
        }
        sendCommands(macro, false);
    });

    $("#btn4").on('click', function() {
        var macro = playerData.players[currentPlayer]['macros']['ActionButton4'];
        if (macro == "") {
            return;
        }
        sendCommands(macro, false);
    });


    $('#sneak').on('click', function(e) {
        autoSneak = !autoSneak;
        var msg;
        if (autoSneak) {
            msg = "You are now auto-sneaking.";
        } else {
            msg = "You are no longer auto-sneaking.";
        }
        $("#text").append("<pre><span style=\"color:white;\">" + msg + "</span><br><br></pre>");
        updateScroll();
    });

    $('#NW').on('click', function(e) {
        var sneak = autoSneak ? "sneak " : "";
        socket.send(sneak + "nw");
    });
    $('#N').on('click', function(e) {
        var sneak = autoSneak ? "sneak " : "";
        socket.send(sneak + "n");
    });
    $('#NE').on('click', function(e) {
        var sneak = autoSneak ? "sneak " : "";
        socket.send(sneak + "ne");
    });
    $('#W').on('click', function(e) {
        var sneak = autoSneak ? "sneak " : "";
        socket.send(sneak + "w");
    });
    $('#E').on('click', function(e) {
        var sneak = autoSneak ? "sneak " : "";
        socket.send(sneak + "e");
    });
    $('#SW').on('click', function(e) {
        var sneak = autoSneak ? "sneak " : "";
        socket.send(sneak + "sw");
    });
    $('#S').on('click', function(e) {
        var sneak = autoSneak ? "sneak " : "";
        socket.send(sneak + "s");
    });
    $('#SE').on('click', function(e) {
        var sneak = autoSneak ? "sneak " : "";
        socket.send(sneak + "se");
    });


});